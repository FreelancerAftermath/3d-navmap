
import 'babylonjs';
import 'babylonjs-materials';
import Navmap from "../../Navmap";

export default class NavCamera {
    private Navmap: Navmap;
    private Camera: BABYLON.ArcRotateCamera;

    public constructor(map: Navmap) {
        this.Navmap = map;
        this.Camera = null;
    };

    public SetupCamera(): void {
        this.Camera = new BABYLON.ArcRotateCamera('mainCamera', Math.PI / 2, Math.PI / 3.2, 2, BABYLON.Vector3.Zero(), this.Navmap.Scene);
        this.Camera.attachControl(this.Navmap.DocumentView); 
        this.Camera.panningSensibility = 30;
        this.Camera.wheelPrecision = 0.5;

        // Starting Animation, ease in camera
        const positionAnimation = new BABYLON.Animation("camPos", "position", 30, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
        positionAnimation.setKeys([{
            frame : 0,
            value : new BABYLON.Vector3(0, 3000, 3600)
        }, {
            frame : 100,
            value : new BABYLON.Vector3(360 * 8, 2500, 1200)
        }]);
        this.Camera.animations.push(positionAnimation);
        this.Navmap.Scene.beginAnimation(this.Camera, 0, 100, false, 1);
    }

    public Clamp(inSystem: Boolean): void {
        if (inSystem) {
            this.Camera.upperRadiusLimit = 600;
            this.Camera.lowerRadiusLimit = null;
        } 
        else {
            this.Camera.lowerRadiusLimit = 600;
            this.Camera.upperRadiusLimit = null;
        }
    }

    public GetCameraRotation(): BABYLON.Vector3 {
        return this.Camera.rotation;
    }
}