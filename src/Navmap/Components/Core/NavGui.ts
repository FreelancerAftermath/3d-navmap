import 'babylonjs'
import 'babylonjs-gui'

import Navmap from '../../Navmap';
import { Pair } from '../../Utils';
import System from '../Views/System';

export default class NavGui {
    private Navmap: Navmap;

    private StarSelectionPanel: BABYLON.GUI.StackPanel;
    private StarSelectionText: BABYLON.GUI.TextBlock;
    private StarSelectionPlane: BABYLON.Mesh;

    constructor(map: Navmap) {
       this.Navmap = map;

       this.StarSelectionPlane = BABYLON.Mesh.CreatePlane("StarSelectionPlane", 1024, this.Navmap.Scene, true);
       this.StarSelectionPlane.renderingGroupId = 3;
       const dynTex = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(this.StarSelectionPlane, 1024, 1024);

       this.StarSelectionPanel = new BABYLON.GUI.StackPanel();
       this.StarSelectionPanel.adaptHeightToChildren = true;
       this.StarSelectionPanel.adaptWidthToChildren = true;
       this.StarSelectionPanel.alpha = 0;
       dynTex.addControl(this.StarSelectionPanel);
       
       this.StarSelectionText = new BABYLON.GUI.TextBlock("StarSelectionText");
       this.StarSelectionText.resizeToFit = true;
       this.StarSelectionText.fontSize = 30;
       this.StarSelectionText.text = "%system%"
       this.StarSelectionText.color = "#ffffff"
       this.StarSelectionPanel.addControl(this.StarSelectionText);
    }

    public DisplaySystem(pair: Pair<System, BABYLON.Sprite[]>): void {
        this.StarSelectionText.text = String(pair.Key.Data.Name);

        this.StarSelectionPlane.position = pair.Value[0].position.clone();
        this.StarSelectionPlane.position.x += 100;
        this.StarSelectionPanel.alpha = 1;
    }

    public DeselectSystem() {
        this.StarSelectionPanel.alpha = 0;
	}
}