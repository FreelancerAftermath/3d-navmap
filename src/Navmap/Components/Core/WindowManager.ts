import Navmap from "../../Navmap";
import { Vector3, DeepImmutable } from "babylonjs";

export default class WindowManager {
    Navmap: Navmap;
    constructor(map: Navmap) {
        this.Navmap = map;

        // Events
        window.addEventListener('resize', this.OnWindowResize);
        window.addEventListener('contextmenu', (e) => e.preventDefault());

        // Scene
        this.Navmap.Scene.onPointerDown = this.OnPointerDown;
        this.Navmap.Scene.onPointerMove = this.OnPointerMove;
        this.Navmap.Scene.onKeyboardObservable.add(this.OnKeyboard);
    }

    private OnWindowResize = (): void => {
        this.Navmap.DocumentView.width = window.innerWidth;
        this.Navmap.DocumentView.height = window.innerHeight;
        this.Navmap.Engine.resize();
    }

    private OnKeyboard(info: BABYLON.KeyboardInfo) {
        if (!info.event.ctrlKey && !info.event.shiftKey && info.event.code === "KeyF") {
            // Zoom in on target object
        }
    }

    private OnPointerDown = (evt: PointerEvent, pickInfo: BABYLON.PickingInfo, type: BABYLON.PointerEventTypes): void => {
        // If right click
        if (evt.button === 2) {
            this.Navmap.SystemStars.DeselectSystem();
            this.Navmap.Gui.DeselectSystem();
            return;
        }

        // If not left click
        else if (evt.button !== 0)
            return;

        const pickSpriteInfo = this.Navmap.Scene.pickSprite(evt.x, evt.y);
        console.log("Hi");
        if (!pickSpriteInfo.hit)
            return;

        const sprite = pickSpriteInfo.pickedSprite;

        const distance = pickSpriteInfo.pickedPoint.subtract(sprite.position).length();
        if(distance > 10)
            return;

        this.Navmap.SystemStars.SelectSystem(sprite);
    }

    private OnPointerMove = (evt: PointerEvent, pickInfo: BABYLON.PickingInfo, type: BABYLON.PointerEventTypes): void => {
        const pickSpriteInfo = this.Navmap.Scene.pickSprite(evt.x, evt.y);
        if (!pickSpriteInfo.hit)
            return;

        const sprite = pickSpriteInfo.pickedSprite;

        const distance = pickSpriteInfo.pickedPoint.subtract(sprite.position).length();
        if(distance > 10)
            return;
    };
};