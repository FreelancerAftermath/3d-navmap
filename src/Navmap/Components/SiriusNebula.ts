import 'babylonjs';
import 'babylonjs-materials';
import 'babylonjs-loaders';

import Navmap from '../Navmap';

export default class SiriusNebula {
    public Navmap: Navmap;

    public BarrierNebula: BABYLON.ParticleSystem | BABYLON.GPUParticleSystem;
    public CrowNebula: BABYLON.ParticleSystem | BABYLON.GPUParticleSystem;
    public WalkerNebula: BABYLON.ParticleSystem | BABYLON.GPUParticleSystem;
    public EdgeNebula: BABYLON.ParticleSystem | BABYLON.GPUParticleSystem;

	constructor(map: Navmap) {
        this.Navmap = map;
    }

    LoadNebulae = async () => {
        const capacity = 2000;

        const particleTexture = new BABYLON.Texture("Textures/smokeStock.png", this.Navmap.Scene);

        const barrier = 'barrierNebula',
            crow = 'crowNebula',
            walker = 'walkerNebula',
            edge = 'edgeNebula';

        if (this.Navmap.Options.UseGPU && BABYLON.GPUParticleSystem.IsSupported && false)
        {
            // GPU Rendering
            this.BarrierNebula = new BABYLON.GPUParticleSystem(barrier, { capacity: capacity * 20 }, this.Navmap.Scene);
            this.CrowNebula = new BABYLON.GPUParticleSystem(crow, { capacity: capacity * 20 }, this.Navmap.Scene);
            this.WalkerNebula = new BABYLON.GPUParticleSystem(walker, { capacity: capacity * 20 }, this.Navmap.Scene);
            this.EdgeNebula = new BABYLON.GPUParticleSystem(edge, { capacity: capacity * 20 }, this.Navmap.Scene);
        }

        else {
            this.BarrierNebula = new BABYLON.ParticleSystem(barrier, capacity, this.Navmap.Scene);
            this.CrowNebula = new BABYLON.ParticleSystem(crow, capacity, this.Navmap.Scene);
            this.WalkerNebula = new BABYLON.ParticleSystem(walker, capacity, this.Navmap.Scene);
            this.EdgeNebula = new BABYLON.ParticleSystem(edge, capacity, this.Navmap.Scene);
        }

        const mesh = await BABYLON.SceneLoader.ImportMeshAsync(
            "",
            "Models/untitled (2).glb",
            "",
            this.Navmap.Scene);
        mesh.meshes.forEach((mesh) => mesh.visibility = 0);

        let particleSystem;
        const meshEmitter = new BABYLON.MeshParticleEmitter(mesh.meshes[1]);
        particleSystem = new BABYLON.ParticleSystem("particles", 2500 , this.Navmap.Scene);
        particleSystem.renderingGroupId = 2;

        particleSystem.particleEmitterType = meshEmitter;
        particleSystem.manualEmitCount = particleSystem.getCapacity();
        particleSystem.minEmitBox = new BABYLON.Vector3(-25, 2, -25); // Starting all from
        particleSystem.maxEmitBox = new BABYLON.Vector3(25, 2, 25); // To...

        particleSystem.particleTexture = particleTexture.clone();
        particleSystem.emitter = new BABYLON.Vector3(0, 0, 0);

        particleSystem.color1 = new BABYLON.Color4(0.8, 0.8, 0.8, 0.1);
        particleSystem.color2 = new BABYLON.Color4(.95, .95, .95, 0.15);
        particleSystem.colorDead = new BABYLON.Color4(0.9, 0.9, 0.9, 0.1);
        particleSystem.minSize = 3.5;
        particleSystem.maxSize = 5.0;
        particleSystem.minLifeTime = Number.MAX_SAFE_INTEGER;
        particleSystem.emitRate = 50000;
        particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
        particleSystem.gravity = new BABYLON.Vector3(0, 0, 0);
        particleSystem.direction1 = new BABYLON.Vector3(0, 0, 0);
        particleSystem.direction2 = new BABYLON.Vector3(0, 0, 0);
        particleSystem.minAngularSpeed = -2;
        particleSystem.maxAngularSpeed = 2;
        particleSystem.minEmitPower = 0;
        particleSystem.maxEmitPower = 0;
        particleSystem.updateSpeed = 0.005;

        particleSystem.start();

        // Rendering order
        /*this.BarrierNebula.renderingGroupId = 2;
        this.CrowNebula.renderingGroupId = 2;
        this.WalkerNebula.renderingGroupId = 2;
        this.EdgeNebula.renderingGroupId = 2;

        this.BarrierNebula.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
        this.CrowNebula.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
        this.WalkerNebula.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
        this.EdgeNebula.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;

        // Barrier Nebula

        const testMesh = await BABYLON.SceneLoader.ImportMeshAsync(null, 'Models/untitled (2).glb',
            '', this.Navmap.Scene);
        console.log(testMesh);

        const meshParticleEmitter = new BABYLON.MeshParticleEmitter(testMesh.meshes[0]);
        this.BarrierNebula.particleEmitterType = meshParticleEmitter;
        this.BarrierNebula.color1 = new BABYLON.Color4(0.8, 0.8, 0.8, 0.1);
        this.BarrierNebula.color2 = new BABYLON.Color4(.95, .95, .95, 0.15);
        this.BarrierNebula.colorDead = new BABYLON.Color4(0, 0, 0, 0.0);
        this.BarrierNebula.particleTexture = particleTexture.clone();

        // Size of each particle (random between...
        this.BarrierNebula.minSize = 5;
        this.BarrierNebula.maxSize = 10;

        // Life time of each particle (random between...
        this.BarrierNebula.maxLifeTime = Number.MAX_SAFE_INTEGER;

        // Emission rate
        this.BarrierNebula.emitRate = 20000;

        // Speed
        this.BarrierNebula.minEmitPower = .5;
        this.BarrierNebula.maxEmitPower = 1;
        this.BarrierNebula.updateSpeed = 0.005;
        this.BarrierNebula.start();*/
    }
};