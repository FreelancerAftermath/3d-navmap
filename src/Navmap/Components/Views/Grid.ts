
import 'babylonjs';
import 'babylonjs-materials';

import Navmap from '../../Navmap';

export default class GridControl {
    public Navmap: Navmap;

    private BigGrid: BABYLON.Mesh;
    private BigGridMaterial: BABYLON.GridMaterial;
    private BigGridOverlay: BABYLON.Mesh;
    private BigGridOverlayMaterial: BABYLON.GridMaterial;

    private SystemGrid: BABYLON.Mesh;
    private SystemGridMaterial: BABYLON.GridMaterial;

	constructor(map: Navmap) {
		this.Navmap = map;
        this.CreateBigGrid();
        this.CreateSystemGrid();

        this.BigGrid.renderingGroupId = 2;
        this.BigGridOverlay.renderingGroupId = 2;
        this.SystemGrid.renderingGroupId = 2;
    }
    
    private CreateBigGrid() {
        this.BigGridMaterial = new BABYLON.GridMaterial("BigGridMaterial", this.Navmap.Scene);
        this.BigGridMaterial.majorUnitFrequency = 30;
        this.BigGridMaterial.minorUnitVisibility = 0;
        this.BigGridMaterial.gridRatio = 2;
        this.BigGridMaterial.backFaceCulling = false;
        this.BigGridMaterial.mainColor = new BABYLON.Color3(1, 1, 1);
        this.BigGridMaterial.lineColor = new BABYLON.Color3(1.0, 1.0, 1.0);
        this.BigGridMaterial.opacity = 0.6;
    
        this.BigGrid = BABYLON.Mesh.CreateGround("BigGrid", 2500, 2500, 2, this.Navmap.Scene, false);
        this.BigGrid.material = this.BigGridMaterial;

        this.BigGridOverlayMaterial = new BABYLON.GridMaterial("BigGridMaterialOverlayMaterial", this.Navmap.Scene);
        this.BigGridOverlayMaterial.majorUnitFrequency = 30;
        this.BigGridOverlayMaterial.minorUnitVisibility = 0;
        this.BigGridOverlayMaterial.gridRatio = 100;
        this.BigGridOverlayMaterial.backFaceCulling = false;
        this.BigGridOverlayMaterial.mainColor = new BABYLON.Color3(1, 1, 1);
        this.BigGridOverlayMaterial.lineColor = new BABYLON.Color3(1.0, 1.0, 1.0);
        this.BigGridOverlayMaterial.opacity = 0.7;
    
        this.BigGridOverlay = BABYLON.Mesh.CreateGround("BigGridMaterialOverlay", 2500, 2500, 2, this.Navmap.Scene, false);
        this.BigGridOverlay.material = this.BigGridOverlayMaterial;
    }

    private CreateSystemGrid() {
        this.SystemGridMaterial = new BABYLON.GridMaterial("SystemGridMaterial", this.Navmap.Scene);
        this.SystemGridMaterial.majorUnitFrequency = 16;
        this.SystemGridMaterial.minorUnitVisibility = 0.45;
        this.SystemGridMaterial.gridRatio = 4;
        this.SystemGridMaterial.backFaceCulling = false;
        this.SystemGridMaterial.mainColor = new BABYLON.Color3(1, 1, 1);
        this.SystemGridMaterial.lineColor = new BABYLON.Color3(1.0, 1.0, 1.0);
        this.SystemGridMaterial.opacity = 0.1;
    
        this.SystemGrid = BABYLON.Mesh.CreateGround("SystemGrid", 500, 500, 2, this.Navmap.Scene, false);
        this.SystemGrid.material = this.SystemGridMaterial;
        this.SystemGrid.visibility = 0;
    }
}