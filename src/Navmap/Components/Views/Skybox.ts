import Navmap from "../../Navmap";

export default class Skybox {
	private Navmap: Navmap;

	constructor(map: Navmap) {
		this.Navmap = map;

		const path = "Textures/nebula/";
		const skybox = BABYLON.Mesh.CreateBox("skyBox", 10.0, this.Navmap.Scene);
		const skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.Navmap.Scene);
		skyboxMaterial.backFaceCulling = false;
		skyboxMaterial.disableLighting = true;
		skybox.renderingGroupId = 0;
		skybox.isPickable = false;

		skybox.infiniteDistance = true;
		skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(path + "nebula", this.Navmap.Scene);
		skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
		skybox.material = skyboxMaterial;
	}
}