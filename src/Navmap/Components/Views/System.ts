
import 'babylonjs';
import 'babylonjs-materials';

import Navmap from '../../Navmap';
import Utils from '../../Utils';

export interface SysObject {
    Nickname: String;
    Archetype: String;
    Name: String;
    Orientation: BABYLON.Vector3;
    Position: BABYLON.Vector3;
    Info: String;
    Faction: String;
}

export interface SysData {
    Position: BABYLON.Vector3;
    StarColours: Array<BABYLON.Color4>;
    Objects: Array<SysObject>;
    Scale: Number;
    Nickname: String;
    Info: String;
    Name: String;
}

export default class System {
    Navmap: Navmap;
    Data: SysData;

    private constructor(map: Navmap, data: SysData) {
        this.Navmap = map;
        this.Data = data;
    }

    // Loads all the system files
    public static LoadSystems(map: Navmap): Array<System> {
        // hardcoded for now
        const systems: Array<System> = [];

        const sys1: System = new System(map, {
            Position: new BABYLON.Vector3(400, 0, -400),
            StarColours: [Utils.HexToColor4('#ffffff')],
            Objects: [],
            Scale: 1,
            Nickname: "Test01",
            Info: "I am a test",
            Name: "Omicron Test",
        });

        const sys2: System = new System(map, {
            Position: new BABYLON.Vector3(100, 0, -200),
            StarColours: [Utils.HexToColor4('#ffffff'), Utils.HexToColor4('#FFB90A')],
            Objects: [],
            Scale: 1,
            Nickname: "Test02",
            Info: "I am a test",
            Name: "Sigma-Test",
        });

        const sys3: System = new System(map, {
            Position: new BABYLON.Vector3(100, 100, -254),
            StarColours: [Utils.HexToColor4('#74C9E0'), Utils.HexToColor4('#ffffff'), Utils.HexToColor4('#ffffff')],
            Objects: [],
            Scale: 1,
            Nickname: "Test03",
            Info: "I am a test",
            Name: "fuck you cunt Test",
        });

        const sys4: System = new System(map, {
            Position: new BABYLON.Vector3(-224, -20, -230),
            StarColours: [Utils.HexToColor4('#EB4619')],
            Objects: [],
            Scale: 1,
            Nickname: "Test04",
            Info: "I am a test",
            Name: "Liberty Test",
        });

        const sys5: System = new System(map, {
            Position: new BABYLON.Vector3(100, 344, 200),
            StarColours: [Utils.HexToColor4('#FDFFB2')],
            Objects: [],
            Scale: 1,
            Nickname: "Test05",
            Info: "I am a test",
            Name: "Tau Test",
        });

        systems.push(sys1, sys2, sys3, sys4, sys5);
        return systems;
    }
}