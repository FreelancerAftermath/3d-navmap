import 'babylonjs';
import 'babylonjs-materials';

import Navmap from "../../Navmap";
import System from './System';
import { Pair } from '../../Utils';
import { Color4, Color3, Vector3 } from 'babylonjs';

export default class SystemStars {
    private Navmap: Navmap;
    private Systems: Map<string, Pair<System, Array<BABYLON.Sprite>>>;
    private Manager: BABYLON.SpriteManager;

    private ActiveSystem: Pair<System, Array<BABYLON.Sprite>>;
    private ActiveSystemCone: BABYLON.Mesh;
    private ActiveSystemInnerCone: BABYLON.Mesh;
    private ActiveSystemRing: BABYLON.Mesh;

    constructor(map: Navmap) {
        this.Navmap = map;
        this.Systems = new Map<string, Pair<System, Array<BABYLON.Sprite>>>();
        this.Manager = new BABYLON.SpriteManager("StarSpriteManager", "Textures/star.png", 1000, 256, this.Navmap.Scene);
        this.Manager.isPickable = true;
        this.Manager.renderingGroupId = 3;

        System.LoadSystems(this.Navmap).map((sys) => {
            const sprite1 = this.GenerateSprite(sys, 1);
            const sprite2 = this.GenerateSprite(sys, 2);
            const sprite3 = this.GenerateSprite(sys, 3);

            let sprites = [sprite1, sprite2, sprite3].filter((sprite) => {
                return sprite !== null;
            })

            this.Systems.set(sys.Data.Nickname.toLowerCase(), { Key: sys, Value: sprites });
        })

        this.CreateSelectionMesh();
    }

    private CreateSelectionMesh() {
        // radiusTop : 0, radiusBottom : 5, height : 16, radialSegments : 4,
        // heightSegments : 1, openEnded : false, thetaStart : Float, thetaLength : Float
        this.ActiveSystemCone = BABYLON.MeshBuilder.CreateCylinder("SelectionCone", {
            height: 24,
            diameterTop: 0,
            diameterBottom: 25,
            subdivisions: 4,
            tessellation: 16,
            enclose: true
        }, this.Navmap.Scene);

        const material = new BABYLON.StandardMaterial("SelectionConeMaterial", this.Navmap.Scene);
        material.diffuseColor = Color3.FromHexString('#0DFFFF');
        this.ActiveSystemCone.material = material;
        this.ActiveSystemCone.position = new BABYLON.Vector3(200, 200, 200);
        this.ActiveSystemCone.rotation.x = Math.PI;
        this.ActiveSystemCone.renderingGroupId = 3;

        this.ActiveSystemInnerCone = BABYLON.MeshBuilder.CreateCylinder("SelectionInnerCone", {
            height: 24,
            diameterTop: 0,
            diameterBottom: Math.pow(3.6, 2),
            subdivisions: 4,
            tessellation: 16,
            enclose: true
        }, this.Navmap.Scene)

        const innerMaterial = new BABYLON.StandardMaterial("SelectionConeInnerMaterial", this.Navmap.Scene);
        innerMaterial.diffuseColor = Color3.Black();
        this.ActiveSystemInnerCone.material = innerMaterial;
        this.ActiveSystemInnerCone.position = new BABYLON.Vector3(200, 200, 200);
        this.ActiveSystemInnerCone.rotation.x = Math.PI;
        this.ActiveSystemInnerCone.renderingGroupId = 3;

        this.ActiveSystemRing = BABYLON.MeshBuilder.CreateTorus("SelectionRing", { thickness: 3, diameter: 35 }, this.Navmap.Scene)
        this.ActiveSystemRing.material = material;

        this.ActiveSystemCone.visibility = 0;
        this.ActiveSystemInnerCone.visibility = 0;
        this.ActiveSystemRing.visibility = 0;
        this.ActiveSystemRing.renderingGroupId = 3;
    }

    private GenerateSprite(sys: System, num: Number): BABYLON.Sprite {
        let color = sys.Data.StarColours.shift();
        if (color === undefined)
            return null;

        let sprite = new BABYLON.Sprite(sys.Data.Nickname + "StarSprite" + String(num), this.Manager);
        sprite.color = new BABYLON.Color4(color.r, color.g, color.b, color.a);
        sprite.position = new BABYLON.Vector3(sys.Data.Position.x, sys.Data.Position.y, sys.Data.Position.z);
        sprite.size = 120;

        switch (num) {
            case 1:
                sprite.isPickable = true;
                sprite.name = String(sys.Data.Nickname);
                break;
            case 2:
                sprite.position.x += 15;
                break;
            case 3:
                sprite.position.y += 15;
                break;
        }

        return sprite;
    }

    public SelectSystem(sprite: BABYLON.Sprite): void {
        this.ActiveSystem = this.Systems.get(sprite.name.toLowerCase());
        
        const pos = new BABYLON.Vector3(this.ActiveSystem.Value[0].position.x, this.ActiveSystem.Value[0].position.y, this.ActiveSystem.Value[0].position.z);
        pos.y += 45;
        this.ActiveSystemCone.position = pos;
        this.ActiveSystemInnerCone.position = pos;
        this.ActiveSystemRing.position = this.ActiveSystem.Value[0].position.clone();
        this.ActiveSystemCone.visibility = 1;
        this.ActiveSystemInnerCone.visibility = 1;
        this.ActiveSystemRing.visibility = 1;

        console.log("hihihi");

        this.Navmap.Gui.DisplaySystem(this.ActiveSystem);
    }

    public DeselectSystem(): void {
        this.ActiveSystemCone.visibility = 0;
        this.ActiveSystemInnerCone.visibility = 0;
        this.ActiveSystemRing.visibility = 0;
    }
}