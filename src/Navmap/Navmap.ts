import 'babylonjs';
import 'babylonjs-materials';

import MapOptions from "./options"
import WindowManager from "./Components/Core/WindowManager";
import GridControl from './Components/Views/Grid';
import NavCamera from './Components/Core/NavCamera';
import SystemStars from './Components/Views/SystemStars';
import NavGui from './Components/Core/NavGui';
import SiriusNebula from "./Components/SiriusNebula";
import Skybox from "./Components/Views/Skybox";

export default class Navmap {
    DocumentView: HTMLCanvasElement;
    Engine: BABYLON.Engine;
    Scene: BABYLON.Scene;
    Light: BABYLON.HemisphericLight;
    Camera: NavCamera;
    Options: MapOptions;

    // Controls
    WindowManager: WindowManager;

    // Views
    Grid: GridControl;
    SystemStars: SystemStars;
    Gui: NavGui;
    Nebula: SiriusNebula;
    Skybox: Skybox;

    // Settings
    ViewingSystem: Boolean = false;

    constructor(options: MapOptions) {
        this.Options = options;
        this.DocumentView = document.getElementById('view') as HTMLCanvasElement;
        this.Engine = new BABYLON.Engine(this.DocumentView, true);
        this.Scene = new BABYLON.Scene(this.Engine);
        this.Light = new BABYLON.HemisphericLight('mainLight', new BABYLON.Vector3(-0.2, 0.5, 0.8), this.Scene);
        this.Scene.clearColor = new BABYLON.Color4(0, 0, 0, 1);

        this.Camera = new NavCamera(this);
        this.Camera.SetupCamera();

        // Custom constrols
        this.WindowManager = new WindowManager(this);
        this.Gui = new NavGui(this);
        
        // Initial Rendering
        this.Grid = new GridControl(this);
        this.SystemStars = new SystemStars(this);

        this.Nebula = new SiriusNebula(this);
        this.Nebula.LoadNebulae().then(() => console.log('Nebulae Loaded.'));
        this.Skybox = new Skybox(this);
    };

    RenderLoop = () => {
        this.Camera.Clamp(this.ViewingSystem);

        this.Scene.render();
    };
}
