import { Color3, Color4 } from "babylonjs";

export interface Pair<T, TT> {
    Key: T,
    Value: TT
}

export default class Utils {
    private constructor() {}

    public static RgbaToColor4(r: number, g: number, b: number, a: number): BABYLON.Color4 {
        return new BABYLON.Color4(r / 255, g / 255, b / 255, a / 255);
    }

    public static HexToColor4(hex: string) {
        if (hex.length === 7) {
            hex += "FF";
        }
        return BABYLON.Color4.FromHexString(hex);
    }
}